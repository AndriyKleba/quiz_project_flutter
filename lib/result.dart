import 'package:flutter/material.dart';

class Result extends StatelessWidget {
  final int resultScore;
  final Function reset;

  Result(this.resultScore, this.reset);

  String get resultPhrase {
    String resultText;
    if (resultScore >= 30 && resultScore < 69) {
      resultText = 'Поганенький результат';
    } else if (resultScore >= 70 && resultScore <= 90) {
      resultText = 'Ти можеш якщо хочеш';
    } else if (resultScore > 91 && resultScore <= 200) {
      resultText = 'Кінець чемпіон';
    }
    return resultText;
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        children: <Widget>[
          Text(
            resultPhrase,
            style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Colors.blue,
                fontSize: 50.0),
          ),
          FlatButton(
            child: Text(
              'Restart',
              style: TextStyle(
                  color: Colors.lightGreen,
                  fontWeight: FontWeight.w600,
                  backgroundColor: Colors.yellowAccent,
                  fontSize: 30.0),
            ),
            onPressed: reset,
          )
        ],
        mainAxisAlignment: MainAxisAlignment.center,
      ),
    );
  }
}
