import 'package:flutter/material.dart';

class Question extends StatelessWidget {
  final String questionText;

  Question(this.questionText);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.fromLTRB(20, 30, 20, 10),
      width: double.infinity,
      child: Text(
        questionText,
        style: TextStyle(
            fontSize: 22.0,
            color: Colors.yellow,
            backgroundColor: Colors.blue,
            fontWeight: FontWeight.w800,
            height: 1.0),
        textAlign: TextAlign.center,
      ),
    );
  }
}
