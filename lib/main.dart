import 'package:flutter/material.dart';
import 'package:quiz_project/quiz.dart';
import 'package:quiz_project/result.dart';
import './quiz.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  State createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  final _questions = const [
    {
      'questionText': 'Який улюблений колір',
      'answer': [
        {'text': 'жовтий', 'score': 3},
        {'text': 'білий', 'score': 20},
        {'text': 'чорний', 'score': 15}
      ]
    },
    {
      'questionText': 'Коли падає сніг?',
      'answer': [
        {'text': 'зима', 'score': 21},
        {'text': 'весна', 'score': 10},
        {'text': 'літо', 'score': 54}
      ]
    },
    {
      'questionText': 'Яка температура сонця?',
      'answer': [
        {'text': '12233С', 'score': 32},
        {'text': '24332С', 'score': 12},
        {'text': '245534С', 'score': 23}
      ]
    }
  ];
  int _questionIndex = 0;
  int _totalScore = 0;

  void _resetQuiz() {
    setState(() {
      _questionIndex = 0;
      _totalScore = 0;
    });
  }

  void _answerQuestion(int score) {
    _totalScore += score;
    setState(() => _questionIndex++);
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          backgroundColor: Colors.yellow,
          appBar: AppBar(
            title: Center(child: Text('Quiz')),
          ),
          body: _questionIndex < _questions.length
              ? Quiz(
                  answerQuestion: _answerQuestion,
                  questions: _questions,
                  questionIndex: _questionIndex,
                )
              : Result(_totalScore, _resetQuiz)),
    );
  }
}
